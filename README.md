# Habr News Summarizer

This project aims to train a T5 (Text-to-Text Transfer Transformer) model for summarizing news articles from the popular Russian technology news site, habr.com.

## Overview

The T5 model is a powerful text-to-text transformer model developed by Google AI that can be fine-tuned for various natural language processing tasks, including text summarization. In this project, we leverage the T5 model's capabilities to generate concise and informative summaries of news articles from habr.com.

## Running the Module

To run the module, make sure you have Docker installed on your system.

```bash
docker build -t summarize_habr .
docker run summarize_habr
```

## Contributing

We welcome contributions from the community! Please refer to the CONTRIBUTING.md for more information on how to get involved.

## Repository Methodology

In this repository, we follow a specific methodology to ensure code quality and effective collaboration. Below are the main principles we adhere to:

### Branching

We use the Git Flow branching strategy. The main `main` branch contains the stable version of the code, ready for deployment. New features are developed in separate `feature/*` branches, and bug fixes are made in `hotfix/*` branches. After completing work on a feature or fix, it is merged into the `main` branch.

### Commits

When creating commits, we strive to follow best practices:

- Use meaningful commit messages that describe the changes made.
- Each commit should be atomic and introduce logically related changes.

### Merge Requests

All changes to the `main` branch are made through Merge Requests (MRs). This allows for code review and discussion of the proposed changes before merging. MRs should include:

- A brief description of the changes made.
- References to related tasks or issues.
- Test results (if applicable).

### Continuous Integration (CI)

This project has a CI system set up, which automatically runs linting, tests, and builds on every Pull Request. This helps catch issues early and ensures code quality.

### Code Review

All Merge Requests go through a code review process, where other project members review the code for adherence to standards, best practices, and architectural principles. This helps improve code quality and facilitates knowledge sharing within the team.

### Dependencies and Environment

We use Poetry for managing Python dependencies and virtual environments. All dependencies are listed in `pyproject.toml`, and the `poetry install` command is used to create an isolated environment.

### License

This project is licensed under the MIT License.
