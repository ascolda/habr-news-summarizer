# Contributing to Habr News Summarizer

Thank you for your interest in contributing to this project! We welcome contributions from everyone, whether it's reporting issues, submitting feature requests, or contributing code. Here are some guidelines to help you get started.

## Getting Started

1. Fork the repository on GitLab and clone your fork to your local machine.
2. Install Poetry by following the [official installation guide](https://python-poetry.org/docs/#installation).
3. Install the project dependencies by running `poetry install` in the project directory.

## Development Workflow

1. Create a new branch for your changes: `git checkout -b my-feature-branch`.
2. Make your changes and commit them with descriptive commit messages.
3. Push your changes to your fork: `git push origin my-feature-branch`.
4. Open a merge request on GitLab against the main branch of the upstream repository.

### Pre-commit Hooks

This project uses [pre-commit](https://pre-commit.com/) hooks to maintain code quality and consistency. The hooks are defined in the `.pre-commit-config.yaml` file and include the following:

- **trailing-whitespace**: Checks for trailing whitespace in files.
- **check-json**: Checks json files for parseable syntax.
- **check-toml**: Checks toml files for parseable syntax.
- **check-yaml**: Checks yaml files for parseable syntax.
- **detect-private-key**: Detects the presence of private keys.
- **double-quote-string-fixer**: Replaces double quoted strings with single quoted strings.
- **end-of-file-fixer**: Ensures that files end with a newline.
- **name-tests-test**: Verifies that test files are named correctly.
- **check-added-large-files**: Prevents committing large files.
- **check-merge-conflict**: Checks for merge conflicts.
- **ruff**: Lints and formats Python code using the [Ruff](https://github.com/charliermarsh/ruff) tool.
- **nbqa-black**: Formats Jupyter Notebooks using the [nbQA-black](https://github.com/nbQA-dev/nbQA) tool.
- **clean-dotenv**: Automatically creates an .env.example which creates the same keys as your .env file, but without the values using the [clean-dotenv](https://github.com/hija/clean-dotenv) tool.
- **mypy**: Run mypy type checking

Before committing your changes, run `pre-commit run --all-files` to check your code against these hooks. If any issues are found, you can run `pre-commit run --all-files --show-diff-on-failure` to see the suggested changes.

## Testing

Before submitting a merge request, please ensure that your changes pass all tests. You can run the tests with `poetry run pytest`.

## Coding Style

This project follows the [PEP 8](https://www.python.org/dev/peps/pep-0008/) style guide for Python code. We use the tools mentioned above to enforce coding style and best practices.

## License

By contributing to this project, you agree that your contributions will be licensed under the LICENSE.md file in the root directory of this repository.
