FROM python:3.11-slim

WORKDIR /app

COPY pyproject.toml poetry.lock /app/

RUN pip install poetry

RUN poetry config virtualenvs.create false && \
    poetry install --no-interaction --no-ansi --with dev

#COPY . /app
#
#ENTRYPOINT ["poetry", "run", "python", "summarize_habr/app.py"]
